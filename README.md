Apt Upgrade
===========

This role upgrades servers, that use apt (debian, ubuntu).
- To install all updates - ap os_upgrade.yml -l `<hostname or group>` -K -e 'type=all'
- To install only security updates - ap os_upgrade.yml -l `<hostname or group>` -K
- To update specific packages - `ap os_upgrade.yml -e '{ onetime_packages: [package_name] }'

`

Role Variables
--------------

Variable `type` defines what updates are installed. Set this variable to `all` to install all updates

Author Information
------------------

SudoNet 
Patryk Kuś 
www.SudoNet.pl